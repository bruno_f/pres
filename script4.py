#/usr/bin/env python

from scapy.all import *

#generer paquet et envoie
def paq(ip,tap):
	for i in range(2000,2100):
		rep = Ether() / IP(dst=ip) / TCP(dport=i) #paquet IP
		r,nr = srp(rep,iface=tap) #envoyer le paquet
		rx = r[0][1]    #paquet recu
		tx = r[0][0]    #paquet emis
		rtt = (rx.time-tx.sent_time)*1000       #temps rtt = temps reception - temps emission en s et multiplier par 1000 pour avoir en ms
		print "ping:", rtt, "ms"

		
#paq('192.168.4.1','tap714-1')	
	
#paq('192.168.4.1','tap1501-1')
#paq('192.168.4.3','tap1501-1')
#paq('192.168.4.4','tap1501-1')
