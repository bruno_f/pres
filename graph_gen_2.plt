# Choisir le format de sortie
set terminal png

# Choix du titre
set title "Valeur de la différence de RTT entre le 1er et 2eme paquet"

# Choix du nom des axes
set xlabel "Numéro du test"
set ylabel "Différence (en s)"

# Définir l'intevalle de valeurs pour chaque axe
set xrange [0:100]
set yrange [0:1]

# Adapter le nom du fichier contenant le graphe
set output "graph_id_2.png"

# Adapter le nom du fichier contenant les données
plot "diff_data" using 1:2 with points linetype 6 pointtype 1 title "Différence de RTT entre le 1er et 2eme paquet"