# Choisir le format de sortie
set terminal png

# Choix du titre
set title "title"

# Choix du nom des axes
set xlabel "xlabel"
set ylabel "ylabel"

# Adapter le nom du fichier contenant le graphe
set output "graph_file.png"

# Adapter le nom du fichier contenant les données
plot "data_file" with lines