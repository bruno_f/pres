#!/usr/bin/python

from nepi.execution.resource import ResourceFactory
import sys

if len(sys.argv)!=2:
    print "Error : 1 argument only needed"
    sys.exit(1)

type_id=sys.argv[1] #Some RM Type
rm_type=ResourceFactory.get_resource_type(type_id)

for attr in rm_type.get_attributes():
    print attr.name,":",attr.help,"\n"
