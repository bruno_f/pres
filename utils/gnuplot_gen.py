#!/usr/bin/env python

def data_gen(filename,xlist,ylist):
    f=open(filename,"w")
    limit=min(len(xlist),len(ylist))
    for i in range(0,limit):
        f.write(str(xlist[i])+" "+str(ylist[i])+"\n")
    f.close()
