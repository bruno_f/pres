#!/usr/bin/python
# Ce programme permet d'afficher l'ensemble des RM presents dans NEPI

from nepi.execution.resource import ResourceFactory

for type_id in ResourceFactory.resource_types():
    rm_type=ResourceFactory.get_resource_type(type_id)
    print type_id, ":",rm_type.get_help() 
