#!/usr/bin/env python

from nepi.execution.ec import ExperimentController
import os, time
import sys

def add_node(ec, host, user, pl_user, pl_password):
    node = ec.register_resource("PlanetlabNode")
    ec.set(node, "hostname", host)
    ec.set(node, "username", user)
    if pl_user:
        ec.set(node, "pluser", pl_user)
    if pl_password:
        ec.set(node, "plpassword", pl_password)
    ec.set(node, "cleanHome", True)
    ec.set(node, "cleanProcesses", True)
    return node

def add_ovs(ec, bridge_name, virtual_ip_pref, controller_ip, controller_port, node):
    ovs = ec.register_resource("OVSWitch")
    ec.set(ovs, "bridge_name", bridge_name)
    ec.set(ovs, "virtual_ip_pref", virtual_ip_pref)
    ec.set(ovs, "controller_ip", controller_ip)
    ec.set(ovs, "controller_port", controller_port)
    ec.register_connection(ovs, node)
    return ovs

def add_port(ec, port_name, ovs):
    port = ec.register_resource("OVSPort")
    ec.set(port, "port_name", port_name)
    ec.register_connection(port, ovs)
    return port

def add_tap(ec, ip4, prefix4, pointopoint, node):
    tap = ec.register_resource("PlanetlabTap")
    ec.set(tap, "ip4", ip4)
    ec.set(tap, "prefix4", prefix4)
    ec.set(tap, "pointopoint", pointopoint)
    ec.set(tap, "up", True)
    ec.register_connection(tap, node)
    return tap

def add_tunnel(ec, network, port0, tap):
    tunnel = ec.register_resource("OVSTunnel")
    ec.set(tunnel, "network", network)
    ec.register_connection(port0, tunnel)
    ec.register_connection(tunnel, tap)
    return tunnel

def add_app(ec, command, node):
    app = ec.register_resource("LinuxApplication")
    ec.set(app, "command", command)
    ec.register_connection(app, node)
    return app

def add_app_src(ec, src, command, node):
    app = ec.register_resource("LinuxApplication")
    ec.set(app,"sources",src)
    ec.set(app, "command", command)
    ec.set(app,"sudo",True)
    #ec.set(app,"depends","python python-scapy")
    ec.register_connection(app, node)
    return app

# TODO : add_controler

def add_controler(ec,node,component):
  app = ec.register_resource("LinuxApplication")
  ec.set(app,"depends","python")
  ec.set(app, "command", "~/pox/pox.py "+component)
  ec.register_connection(app, node)
  return app

# controler_node=add_node(ec, controler, slicename, pl_user, pl_password)
# add_controler(ec,controler_node,"samples.pretty_log forwarding.l2_learning")
# Ne pas oublie de configurer le workflow et pourquoi pas de recuperer les traces.
# Ou bien rester moins generaliste envoyer la commande et supposer que pox est bien installe
