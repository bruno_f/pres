#!/usr/bin/python

from nepi.execution.resource import ResourceFactory
import sys

if len(sys.argv)!=2:
    print "Error : 1 argument only needed"
    sys.exit(1)

type_id=sys.argv[1]
rm_type=ResourceFactory.get_resource_type(type_id)

for trace in rm_type.get_traces():
    print trace.name,":",trace.enabled,"\n"

# Permettre a une instance de RM d'avoir un "trace" specifique actif :
# ec.enable_trace(guid,"trace_name")
# print ec.trace_enabled(guid, "trace-name")
