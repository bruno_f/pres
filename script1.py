#/usr/bin/env python

from scapy.all import *
from datetime import *
from time import *
import sys

#generer paquet et envoie
def paq(ip,tap):
	cpt=0
	t1=datetime.utcnow()
	for i in range(2000,2100):
		nr=[]
		rep = Ether() / IP(dst=ip) / TCP(dport=i) #paquet IP
		r,nr = srp(rep,iface=tap,timeout=4) #envoyer le paquet
		if len(nr)!=0:
			cpt=cpt+1
			print "Paquet perdu "+str(cpt)

		if cpt>5 :
			print "DDoS !!!!"
			break
		# rx = r[0][1]    #paquet recu
		# tx = r[0][0]    #paquet emis
		# rtt = (rx.time-tx.sent_time)*1000       #temps rtt = temps reception - temps emission en s et multiplier par 1000 pour avoir en ms
		# print "ping:", rtt, "ms"
	t2=datetime.utcnow()
	d=t2-t1
	print "DDoS realise au bout de : "+str(d.total_seconds())


paq('192.168.4.4','tap1920-0')	
	
#paq('192.168.4.1','tap1310-0')
#paq('192.168.4.2','tap1310-0')
#paq('192.168.4.4','tap1310-0')

#paq('192.168.4.1','tap1073-0')
#paq('192.168.4.2','tap1073-0')
#paq('192.168.4.4','tap1073-0')

#paq('192.168.4.1','tap761-0')
#paq('192.168.4.2','tap761-0')
#paq('192.168.4.4','tap761-0')
