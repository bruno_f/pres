#!/usr/bin/env python
#
# "132.227.62.120"

from nepi.execution.ec import ExperimentController
from utils.add_RM import *
import os, time
import sys

ec = ExperimentController(exp_id = "test_de_bruno")


switch1 = "planetlab2.ionio.gr"
switch2 = "planetlab1.ionio.gr"
host1 = "pl2.uni-rostock.de"
host2 = "dplanet2.uoc.edu"

ip_controller = "130.37.193.143"
network = "192.168.4.0"

slicename = "inria_pres"

pl_user = None
pl_password = None

s1_node = add_node(ec, switch1, slicename, pl_user, pl_password)
s2_node = add_node(ec, switch2, slicename, pl_user, pl_password)

ovs1 = add_ovs(ec, "switch_1", "192.168.4.1/24", ip_controller, "6633", s1_node)
ovs2 = add_ovs(ec, "switch_2", "192.168.4.2/24", ip_controller, "6633", s2_node)

port1 = add_port(ec, "nepi_port1", ovs1)
port3 = add_port(ec, "nepi_port3", ovs1)
port2 = add_port(ec, "nepi_port2", ovs2)
port4 = add_port(ec, "nepi_port4", ovs2)

h1_node = add_node(ec, host1, slicename, pl_user, pl_password)
h2_node = add_node(ec, host2, slicename, pl_user, pl_password)

tap1 = add_tap(ec, "192.168.4.3", 24, "192.168.4.1", h1_node)
tap2 = add_tap(ec, "192.168.4.4", 24, "192.168.4.2", h2_node)

tunnel1 = add_tunnel(ec, network, port1, tap1)
tunnel2 = add_tunnel(ec, network, port2, tap2)
tunnel3 = add_tunnel(ec, network, port3, port4)

app1 = add_app_src(ec, "/home/bruno/pres/python/msr_rtt.py", "python ${SRC}/msr_rtt.py 192.168.4.3 192.168.4.3 2020 3030 "+ec.get(tap1,"deviceName"), h1_node)

ec.deploy()

ec.wait_finished([app1])

mytrace1 = ec.trace(app1, 'stdout')

f = open("/home/bruno/pres/python/traces/info_disclosure.trace", 'w')
f.write("************ MyTrace 1  ********************\n\n")
f.write(mytrace1)
f.close()

# Delete the overlay network
ec.shutdown()
