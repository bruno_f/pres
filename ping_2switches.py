#!/usr/bin/env python

from nepi.execution.ec import ExperimentController
from utils.add_RM import *
import os, time
import sys

# Create the EC
ec = ExperimentController(exp_id = "test_de_bruno")

switch1 = "planetlab2.virtues.fi"
switch2 = "planetlab2.cs.aueb.gr"
host1 = "planetlab1.di.fct.unl.pt"
host2 = "iraplab2.iralab.uni-karlsruhe.de"

ip_controller = "132.227.62.120"
network = "192.168.4.0"

slicename = "inria_pres"

pl_user = None
pl_password = None

s1_node = add_node(ec, switch1, slicename, pl_user, pl_password)
s2_node = add_node(ec, switch2, slicename, pl_user, pl_password)

# Add switches 
ovs1 = add_ovs(ec, "switch_1", "192.168.4.1/24", ip_controller, "6633", s1_node)
ovs2 = add_ovs(ec, "switch_2", "192.168.4.2/24", ip_controller, "6633", s2_node)

# Add ports on ovs
port1 = add_port(ec, "nepi_port1", ovs1)
port3 = add_port(ec, "nepi_port3", ovs1)
port2 = add_port(ec, "nepi_port2", ovs2)
port4 = add_port(ec, "nepi_port4", ovs2)

h1_node = add_node(ec, host1, slicename, pl_user, pl_password)
h2_node = add_node(ec, host2, slicename, pl_user, pl_password)

# Add tap devices
tap1 = add_tap(ec, "192.168.4.3", 24, "192.168.4.1", h1_node)
tap2 = add_tap(ec, "192.168.4.4", 24, "192.168.4.2", h2_node)

# Connect the nodes
tunnel1 = add_tunnel(ec, network, port1, tap1)
tunnel2 = add_tunnel(ec, network, port2, tap2)
tunnel3 = add_tunnel(ec, network, port3, port4)

# Add ping commands
app1 = add_app(ec, "ping -c5 192.168.4.2", s1_node)
app2 = add_app(ec, "ping -c5 192.168.4.3", s1_node)
app3 = add_app(ec, "ping -c5 192.168.4.4", s1_node)
app4 = add_app(ec, "ping -c5 192.168.4.1", s2_node)
app5 = add_app(ec, "ping -c5 192.168.4.3", s2_node)
app6 = add_app(ec, "ping -c5 192.168.4.4", s2_node)
app7 = add_app(ec, "ping -c5 192.168.4.1", h1_node)
app8 = add_app(ec, "ping -c5 192.168.4.2", h1_node)
app9 = add_app(ec, "ping -c5 192.168.4.4", h1_node)
app10 = add_app(ec, "ping -c5 192.168.4.1", h2_node)
app11 = add_app(ec, "ping -c5 192.168.4.2", h2_node)
app12 = add_app(ec, "ping -c5 192.168.4.3", h2_node)

ec.deploy()

ec.wait_finished([app1, app2, app3, app4, app5, app6, app7, app8, app9, app10, app11, app12])

# Retreive ping results and save
# them in a file
ping1 = ec.trace(app1, 'stdout')
ping2 = ec.trace(app2, 'stdout')
ping3 = ec.trace(app3, 'stdout')
ping4 = ec.trace(app4, 'stdout')
ping5 = ec.trace(app5, 'stdout')
ping6 = ec.trace(app6, 'stdout')
ping7 = ec.trace(app7, 'stdout')
ping8 = ec.trace(app8, 'stdout')
ping9 = ec.trace(app9, 'stdout')
ping10 = ec.trace(app10, 'stdout')
ping11 = ec.trace(app11, 'stdout')
ping12 = ec.trace(app12, 'stdout')

if not ping12:
  ec.shutdown()
  sys.exit("No ping found")

f = open("~/pres/python/traces/ping_2switches_2.trace", 'w')
f.write("************ Ping From Switch 1 : 192.168.4.1 ********************\n\n")
f.write(ping1)
f.write("----------------------------------------\n\n")
f.write(ping2)
f.write("----------------------------------------\n\n")
f.write(ping3)
f.write("************ Ping From Switch 2 : 192.168.4.2 ********************\n\n")
f.write(ping4)
f.write("----------------------------------------\n\n")
f.write(ping5)
f.write("----------------------------------------\n\n")
f.write(ping6)
f.write("************ Ping From Host 1 : 192.168.4.3 ********************\n\n")
f.write(ping7)
f.write("----------------------------------------\n\n")
f.write(ping8)
f.write("----------------------------------------\n\n")
f.write(ping9)
f.write("************ Ping From Host 2 : 192.168.4.4 ********************\n\n")
f.write(ping10)
f.write("----------------------------------------\n\n")
f.write(ping11)
f.write("----------------------------------------\n\n")
f.write(ping12)

f.close()

# Delete the overlay network
ec.shutdown()
