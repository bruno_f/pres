#!/usr/bin/python

# 1) Mesure du temps au depart
# 2) Envoi d'un paquet vers lui meme avec sr1
# 3) Mesure du temps a l'arrivee
# 4) Envoyer le meme paquet et repeter les etapes 1 a 3
# 5) Comparer les deux duree

from scapy.all import *
from datetime import *
from time import *
import sys

def pqt_gen(ipsrc,ipdst,tcpsrc,tcpdst,ifce):
    eth=Ether()
    ip=IP(src=ipsrc,dst=ipdst)
    tcp=TCP(sport=tcpsrc,dport=tcpdst)
    pqt=eth/ip/tcp
    # pqt.show()
    ack,nack=srp(pqt,iface=ifce,timeout=4)
    return nack

if len(sys.argv)!=6:
    print "Error : 5 argument needed"
    sys.exit(1)

src=sys.argv[1]
dst=sys.argv[2]
sport=int(sys.argv[3])
dport=int(sys.argv[4])
iface=sys.argv[5]

list1=[]
list2=[]


for i in range(0,100):
    # Envoi du 1er paquet
    t1=datetime.utcnow()
    nack=pqt_gen(src,dst,sport,dport+i,iface)
    t2=datetime.utcnow()
    if len(nack)!=0:
        print "Paquet non recu"
    # d1 : RTT pour le 1er paquet
    d1=t2-t1

    # Envoi du 2eme paquet
    t1=datetime.utcnow()
    pqt_gen(src,dst,sport,dport+i,iface)
    t2=datetime.utcnow()

    # d2 : RTT pour le 2eme paquet
    d2=t2-t1

    list1.append(d1)
    list2.append(d2)

for i in range(0,len(list1)):
 print str(i)+" "+str(list1[i].total_seconds())

print ""

for i in range(0,len(list2)):
 print str(i)+" "+str(list2[i].total_seconds())

